<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StudyTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $this->visit('/')
             ->see('Lista de estudios');
    }

    public function testCreate()
{
    $this->visit('/study/index')
        ->see('Lista de estudios')
        ->see('Añadir estudio')
        ->click('Añadir estudio')
        ->seePageIs('/study/new')
        ->see('Añadir estudio')
        ->see('Añadir')
        ->type('IFC302', 'code')
        ->type('Técnico Superior en Desarrollo de aplicaciones Multiplaforma', 'name')
        ->type('Desarrollo de aplicaciones multiplataforma', 'shortName')
        ->type('DAM', 'abv')
        ->press('Añadir')
        ->seePageIs('/study')
        ->see('IFC303')
        ;
}
}
