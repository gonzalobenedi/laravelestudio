<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StudiesModules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules_studies', function (Blueprint $table) {
            $table->integer('study_id')->unsigned()->nullable();
            $table->foreign('study_id')->references('id')
            ->on('studies')->onDelete('cascade');

            $table->integer('module_id')->unsigned()->nullable();
            $table->foreign('module_id')->references('id')
            ->on('modules')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
