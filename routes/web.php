<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// RUTAS DE STUDY

Route::get('/', 'StudyController@index');

Route::get('/study/index/{page?}', 'StudyController@index')
->where('page', '[0-9]+');

Route::get('/study/delete/{id}', 'StudyController@delete');

Route::get('/study/new', 'StudyController@new');

Route::post('/study/insert', 'StudyController@insert');

Route::get('/study/edit/{id}', 'StudyController@edit');

Route::post('/study/update', 'StudyController@update');

Route::get('/study/details/{id}', 'StudyController@details');

Route::get('/module/search', 'StudyController@search');

Route::post('/module/find', 'StudyController@find');

// RUTAS DE MODULE

Route::get('/module/index/{page?}', 'ModuleController@index')
->where('page', '[0-9]+');

Route::get('/module/delete/{id}', 'ModuleController@delete');

Route::get('/module/new', 'ModuleController@new');

Route::post('/module/insert', 'ModuleController@insert');

Route::get('/module/edit/{id}', 'ModuleController@edit');

Route::post('/module/update', 'ModuleController@update');

Route::get('/module/search', 'ModuleController@search');

Route::post('/module/find', 'ModuleController@find');

// RUTAS DE LOGIN

Route::get('/login/in', 'LoginController@in');

Route::get('/login/out', 'LoginController@out');

Route::post('/login/save', 'LoginController@save');

// RUTAS DE USER

Route::get('/user/index/{id}', 'UserController@index');
Route::get('/user/register', 'UserController@register');
Route::post('/user/save', 'UserController@save');

// RUTA DE HELP

Route::get('/help','HelpController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');
