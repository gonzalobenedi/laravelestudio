@extends('layout.app')
@section('title', 'Matricula Laravel')

@section('content')
  <h2>Formulario de registro</h2>
  <br>

  <form class="" action="/user/save" method="post">
      {{ csrf_field() }}
      <fieldset>
      <p><label>Nombre de usuario:</label> <input type="text" name="name" value="" autofocus="true"></p>
      <p><label>Contraseña:</label> <input type="password" name="password" value=""></p>
      <p><label>Conf. contraseña: </label> <input type="password" name="repeat_password" value=""></p>
      <p><label>Email:</label> <input type="text" name="email" value=""></p>

      <p>
        <input type="submit" name="submit" value="Añadir">
        <input type="button" name="button" value="Cancelar" onclick="history.go(-1)">
      </p>
      </fieldset>
  </form>
@endsection
