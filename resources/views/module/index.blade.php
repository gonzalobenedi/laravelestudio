@extends('layout.app')
@section('title', 'Matrícula Laravel')

@section('content')
  <h2>Lista de módulos</h2>
  <br>
  <p>
    <a href="/module/new"><i class="fa fa-plus"></i> Añadir módulo</a> |
    <a href="/module/search"><i class="fa fa-search"></i> Buscar módulo</a>
  </p>
  <br>
  @if (count($modules)==0)
    <h3>No hay módulos en la base de datos.</h3>
  @else
    <table>
      <tr>
        <th>Code</th>
        <th>Nombre</th>
        {{-- <th>Horas</th> --}}
        <th class="center">Editar</th>
        <th class="center">Eliminar</th>
      </tr>

      @foreach ($modules as $module)
        <tr>
          <td>{{ $module["code"] }}</td>
          <td>{{ $module["name"] }}</td>
          {{-- <td>{{ $module["HOURS"] }}</td> --}}
          <td class="center"><a href="/module/edit/{{$module["id"]}}"><i class="fa fa-pencil-square"></i></a></td>
          <td class="center"><a href="/module/delete/{{$module["id"]}}"><i class="fa fa-trash"></i></a></td>
        </tr>
      @endforeach
    </table>
  @endif
@endsection
