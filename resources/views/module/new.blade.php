@extends('layout.app')
@section('title',"Matrícula Laravel")

@section('content')
    <h2>Añadir módulo</h2>
    <form class="" action="/module/insert" method="post">
      {{ csrf_field() }}
      <fieldset>
      <p><label>Código:</label> <input type="text" name="code" value="" autofocus="true"></p>
      <p><label>Nombre:</label> <input type="text" name="name" value=""></p>
      {{-- <p><label>Horas:</label> <input type="number" name="hours" value=""></p> --}}
      <p><label>Estudio:</label><select name="study">
        @foreach ($studies  as $study)
            <option value="{{$study["id"]}}">{{$study["name"]}}</option>
        @endforeach
      </select></p>
      <p>
        <input type="submit" name="submit" value="Añadir">
        <input type="button" name="button" value="Cancelar" onclick="history.go(-1)">
      </p>
      </fieldset>
    </form>
@endsection
