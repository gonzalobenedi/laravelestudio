@extends('layout.app')
@section('title',"Matricula Laravel")

@section('content')
  <h2>Editar módulo</h2>
    <form class="" action="/module/update" method="post">
      {{ csrf_field() }}
      <fieldset>
      <p><label>Código:</label> <input type="text" name="code" value="{{$module["code"]}}" autofocus="true"></p>
      <p><label>Nombre:</label> <input type="text" name="name" value="{{$module["name"]}}"></p>
      {{-- <p><label>Horas:</label> <input type="number" name="hours" value="{{$module["HOURS"]}}"></p> --}}
      <input type="hidden" name="id" value="{{$module["id"]}}">
      <p><label>Estudio:</label><select name="study">
        <option value="DAM">Desarrollo de aplicaciones multiplataforma</option>
        <option value="DAW">Desarrollo de aplicaciones web</option>
      </select></p>
      <p>
        <input type="submit" name="submit" value="Actualizar">
        <input type="button" name="button" value="Cancelar" onclick="history.go(-1)">
      </p>
      </fieldset>
    </form>
@endsection
