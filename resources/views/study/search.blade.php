@extends('layout.app')
@section('title',"Matricula Laravel")

@section('content')
  <h2>Buscar estudio</h2>
    <form class="" action="/study/find" method="post">
      {{ csrf_field() }}
      <p><label>Nombre:</label> <input type="text" name="name" value=""></p>
      <p>
        <input type="submit" name="submit" value="Buscar">
        <input type="button" name="button" value="Cancelar" onclick="history.go(-1)">
      </p>
    </form>
@endsection
