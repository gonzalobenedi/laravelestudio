@extends('layout.app')
@section('title', 'Matricula Laravel')

@section('content')
  <h2>Lista de estudios</h2>
  <br>
  <p>
      @if (session("user")!="")
          <a href="/study/new"><i class="fa fa-plus"></i> Añadir estudio</a> |
      @endif

    <a href="/study/search"><i class="fa fa-search"></i> Buscar estudio</a>
  </p>
  <br>
  <table>
    <tr>
      <th>Code</th>
      <th>Nombre</th>
      <th>Nombre Corto</th>
      <th>Abreviatura</th>
      @if (session("user")!="")
          <th class="center">Editar</th>
          <th class="center">Eliminar</th>
      @endif
    </tr>
    @foreach ($studies as $study)
       <tr>
        <td><a href="/study/details/{{$study["id"]}}">{{ $study["code"] }}</a></td>
        <td>{{ $study["name"] }}</td>
        <td>{{ $study["shortName"] }}</td>
        <td>{{ $study["abreviation"] }}</td>
        @if (session("user")!="")
            <td class="center"><a href="/study/edit/{{$study["id"]}}"><i class="fa fa-edit"></i></a></td>
            <td class="center"><a href="/study/delete/{{$study["id"]}}"><i class="fa fa-trash"></i></a></td>
        @endif
      </tr>
    @endforeach
  </table>
  <div class="paginator">
      Páginas: {{$studies->links()}}
  </div>

@endsection
