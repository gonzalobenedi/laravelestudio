@extends('layout.app')
@section('title', 'Matricula Laravel')

@section('content')
    <h2>Detalles del estudio</h2>
    <br>
    <p><b>Código: </b>{{$study["code"]}}</p>
    <p><b>Nombre: </b>{{$study["name"]}}</p>
    <p><b>Nombre corto: </b>{{$study["shortName"]}}</p>
    <p><b>Abreviatura: </b>{{$study["abreviation"]}}</p>
    <br>
    <h3>Módulos que contiene:</h3><br>
    <table>
        <tr>
            <th>Código</th>
            <th>Nombre</th>
            <th>Curso</th>
            <th>Horas semanales</th>
            <th>Horas totales</th>
        </tr>
    </table>
@endsection
