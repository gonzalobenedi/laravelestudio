@extends('layout.app')
@section('title','Matrícula Laravel')

@section('content')
  <h2>Editar estudio</h2>
  <form class="" action="/study/update" method="post">
    {{ csrf_field() }}
    <fieldset>
    <p><label>Código:</label> <input type="text" name="code" value="{{$study["code"]}}" autofocus="true"></p>
    <p><label>Nombre:</label> <input type="text" name="name" value="{{$study["name"]}}"></p>
    <p><label>Nombre corto:</label> <input type="text" name="shortName" value="{{$study["shortName"]}}"></p>
    <p><label>Abreviatura:</label> <input type="text" name="abv" value="{{$study["abreviation"]}}"></p>
    <input type="hidden" name="id" value="{{$study["id"]}}">
    <p>
      <input type="submit" name="submit" value="Modificar">
      <input type="button" name="button" value="Cancelar" onclick="history.go(-1)">
    </p>
    </fieldset>
  </form>
@endsection
