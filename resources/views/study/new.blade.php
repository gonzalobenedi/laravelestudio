@extends('layout.app')
@section('title',"Matrícula Laravel")

@section('content')
    <h2>Añadir estudio</h2>
    <form class="" action="/study/insert" method="post">
      {{ csrf_field() }}
      <fieldset>
      <p><label>Código:</label> <input type="text" name="code" value="" autofocus="true"></p>
      <p><label>Nombre:</label> <input type="text" name="name" value=""></p>
      <p><label>Nombre corto:</label> <input type="text" name="shortName" value=""></p>
      <p><label>Abreviatura:</label> <input type="text" name="abv" value=""></p>
      <p>
        <input type="submit" name="submit" value="Añadir">
        <input type="button" name="button" value="Cancelar" onclick="history.go(-1)">
      </p>
      </fieldset>
    </form>

    @foreach ($errors->all() as $error)
        <li>{{$error}}</li>
    @endforeach
@endsection
