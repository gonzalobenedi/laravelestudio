<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!--CSS Link-->
    <link rel="stylesheet" type="text/css" href="/css/default.css">
    <!--Font Awesome Link-->
    <link href="/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Oswald:300" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
    <!-- jQuery -->
    <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
    {{-- Script --}}
    <script type="text/javascript" src="/js/script.js"></script>
    <title>@yield('title')</title>
</head>
<body>
    <!-- Menú -->
    <header>
        <div class="menu_bar">
            <a href="#" class="bt-menu"><span id="btn" class="fa fa-bars"></span>Matrícula Laravel</a>
        </div>
        <div class="title">
            <h1>Matrícula Laravel</h1>
        </div>
        <nav>
            <ul>
                <li><a href="/study/index"><span class="fa fa-graduation-cap"></span>Estudios</a></li>
                <li><a href="/module/index"><span class="fa fa-book"></span>Módulos</a></li>
                <li ><a href="/help"><span class="fa fa-info"></span>Ayuda</a></li>
            </ul>
        </nav>
    </header>
    <div class="user">
        @if (session("user") == "")
            <p>Hola Invitado | <a href="/login/in"><i class="fa fa-sign-in"></i> Login </a> | <a href="/user/register"><i class="fa fa-user-plus"></i> Registrarse</a></p>
        @else
            <p>Hola {{session("user")}} | <a href="/login/out"><i class="fa fa-sign-out"></i> Logout </a></p>
        @endif

    </div>
    <div class="content">
      @yield('content')
    </div>
    <div id="footer">
          <ul>
              <li><a href="#"><i class="fa fa-github fa-2x"></i></a></li>
              <li><a href="#"><i class="fa fa-bitbucket fa-2x"></i></a></li>
              <li><a href="#"><i class="fa fa-linkedin fa-2x"></i></a></li>
              <li class="last"><a href="#"><i class="fa fa-envelope fa-2x"></i></a></li>
          </ul>
         <div class="copyright">
             <i class="fa fa-copyright"></i> Gonzalo Benedí 2017
         </div>
    </div>
</body>
</html>
