@extends('layout.app')
@section('title', 'Matricula Laravel')

@section('content')
  <h2>Login</h2>
  <br>
  <form class="" action="/login/save" method="post">
      {{ csrf_field() }}
      <fieldset>
          <p><label>Nombre de usuario: </label><input type="text" name="user" value=""></p>
          <p><label>Contraseña: </label><input type="password" name="password" value=""></p>
          <input type="submit" name="" value="Entrar">
      </fieldset>
  </form>
@endsection
