<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    public function studies()
    {
        return $this->belongsToMany('App\Study')
      ->withTimestamps();
    }
}
