<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function index($id)
    {
        # code...
    }

    public function register()
    {
        return view("/user/register");
    }

    public function save(Request $request)
    {
        if ($request->password == $request->repeat_password) {
            $user = new User;

            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = $request->password;

            $user->save();

            return redirect('/study/index');
        }
    }
}
