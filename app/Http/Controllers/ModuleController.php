<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Module;
use App\Study;

class ModuleController extends Controller
{
  function __construct()
  {
    # code...
  }

  public function index($page=1)
  {
    $modules = Module::all();
    //dd($modules);
    return view('module/index',[
      "page"=>$page,
      "modules"=>$modules
    ]);
  }

  public function delete($id)
  {
    // Eliminará el mudulo de $id e ira a index
    Module::destroy($id);
    return redirect('/module/index');
  }

  public function new()
  {
    $studies = Study::all();
    return view('module/new',[
        "studies"=>$studies
    ]);
  }

  public function insert(Request $request)
  {
    // Insertará el modulo en la bbdd con los datos que le llegan en $request
    $module = new Module;

    $module->code = $request->code;
    $module->name = $request->name;

    $module->save();
    $module->studies()->save(Study::find($request->study));
    return redirect('/module/index');
  }

  public function edit($id)
  {
    // Coger el modulo con el id $id y mandarlo a la view edit con sus datos
    $module = Module::find($id);
    return view('module/edit',[
      "module"=>$module['attributes']
    ]);
  }

  public function update(Request $request)
  {
    // Actualizará el modulo en la bbdd con los datos que le llegan en $request
    $module = Module::find($request->id);
    $module->code = $request->code;
    $module->name = $request->name;

    $module->save();

    return redirect('/module/index');
  }

  public function search()
  {
    return view('module/search');
  }

  public function find(Request $request)
  {
    $modules = Module::where('name', 'like',"%".$request->name."%")->get();

    return view('module/index',[
      "modules"=>$modules
    ]);
  }
}
