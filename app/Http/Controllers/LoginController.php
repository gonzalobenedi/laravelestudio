<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Session;
use App\User;

class LoginController extends Controller
{
    public function in()
    {
        return view('/login/index');
    }

    public function save(Request $request)
    {
        $count = User::where([
            ['name','=',$request->user],
            ['password','=',$request->password]
        ])->count();
        if ($count > 0) {
            Session::put("user", $request->user);
        }
        return redirect('/study/index');
    }

    public function out()
    {
        session()->forget("user");
        return redirect('/study/index');
    }
}
