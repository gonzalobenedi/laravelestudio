<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Study;

class StudyController extends Controller
{
  function __construct()
  {
    # code...
  }

  public function index($page=1)
  {
    $studies = Study::paginate(15);
    return view('study/index',[
      "page"=>$page,
      "studies"=>$studies
    ]);
  }

  public function delete($id)
  {
    // Eliminará el estudio de $id e ira a index
    Study::destroy($id);
    return redirect('/study/index');
  }

  public function new()
  {
    return view('study/new');
  }

  public function insert(Request $request)
  {
    // Insertará el estudio en la bbdd con los datos que le llegan en $request
    $this->validate($request, [
        'code' => 'required | unique:studies',
        'name' => 'required',
        'shortName' => 'required',
        'abreviation' => 'required'
    ]);
    $study = new Study;

    $study->code = $request->code;
    $study->name = $request->name;
    $study->shortName = $request->shortName;
    $study->abreviation = $request->abv;

    $study->save();

    return redirect('/study/index');
  }

  public function edit($id)
  {
    $study = Study::find($id);
    return view('study/edit',[
      "study"=>$study["attributes"]
    ]);
  }

  public function update(StudyRequest $request)
  {
    // Actualizará el estudio en la bbdd con los datos que le llegan en $request

    $study = Study::find($request->id);
    $study->code = $request->code;
    $study->name = $request->name;
    $study->shortName = $request->shortName;
    $study->abreviation = $request->abv;

    $study->save();

    return redirect('/study/index');
  }

  public function details($id)
  {
      $study = Study::find($id);
      return view("/study/details",[
          "study"=>$study
      ]);
  }
}
