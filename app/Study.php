<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Study extends Model
{
    public function modules()
    {
        return $this->belongsToMany('App/Module')
        ->withTimestamps();
    }

    public function family()
    {
        return $this->blongsTo("App\Family");
    }
}
